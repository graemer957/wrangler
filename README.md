# wrangler

Docker Image based on the [wrangler CLI](https://github.com/cloudflare/wrangler) used to control [Cloudflare Workers](https://developers.cloudflare.com/workers/quickstart).

## Usage

### Via the command line

```bash
docker run --rm \
	-e CF_ACCOUNT_ID=<Cloudflare account_id> \
	-e CF_API_TOKEN=<Cloudflare API token> \
	registry.gitlab.com/graemer957/wrangler \
	<command>
```

For example, if my account ID was "123" and my API token was "abc" and I wanted to check my credentials:

```bash
docker run --rm \
	-e CF_ACCOUNT_ID=123 \
	-e CF_API_TOKEN=abc \
	registry.gitlab.com/graemer957/wrangler \
	whoami
```

### As part of a build process using a `Dockerfile`

```yml
# Build website using Hugo
FROM registry.gitlab.com/graemer957/hugo AS builder

WORKDIR /site
COPY . .
RUN hugo

# Deploy to Cloudflare Worker using wrangler
FROM registry.gitlab.com/graemer957/wrangler

ARG CF_ACCOUNT_ID
ARG CF_API_TOKEN
ARG CF_ZONE_ID
ARG SITE

WORKDIR /site
COPY --from=builder "site/dist" "dist/"
COPY --from=builder "site/workers-site" "workers-site/"
COPY --from=builder "site/wrangler.toml" "."

# Website content
COPY --from=builder "site/public" "public/"

RUN wrangler publish -e $SITE
```
