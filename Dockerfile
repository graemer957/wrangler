FROM node:lts-alpine

LABEL maintainer="Graeme Read <graeme@sigma957.net>"
LABEL description="Cloudflare's worker CLI wrangler in a convenient Docker Image"

RUN apk add --update ca-certificates
RUN node -v
RUN npm -v
RUN npm i @cloudflare/wrangler -g --unsafe-perm=true

ENTRYPOINT ["wrangler"]
CMD ["-V"]
